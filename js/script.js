

$(function () {
    var $uploadCrop;
    // var IMG_INPUT;


    //按鈕按下上傳照片
    initImgUploader();

    //初始化croppie
    initCroppie();

    //調整照片完成、下載
    initUploadActions();



    function initImgUploader() {
        $('#uploaderInput').on('change', function (e) {
            $('.js-upload-loading').addClass('active');
            loadImage(
                e.target.files[0],
                function (img) {
                    $uploadCrop.croppie('bind', {
                        url: img.toDataURL()
                    }).then(function () {
                        $('.js-upload-loading').removeClass('active');
                        $('.js-step1').removeClass('active')
                        $('.js-step2').addClass('active');
                    });
                },
                {
                    orientation: true
                }
            )
        });
    }

    function initCroppie() {
        $uploadCrop = $('#uploadCroppie').croppie({
            viewport: {
                width: 370,
                height: 370,
            },
            boundary: {
                width: 600,
                height: 600
            }
        });
    }


    var _resultCanvas;
    function initUploadActions() {
        $('#uploadComplete').on('click', function () {
            $uploadCrop.croppie('result', {
                type: 'base64',
                format: 'png',
                size: { width: 1200, height: 1200 }
            }).then(function (result) {
                $('#uploaderImgCropped').attr('src', result);
                $('#uploaderImgCropped').addClass('active');

                //移除croppie
                $('#uploadCroppie').remove();
                $uploadCrop.croppie('destroy');

                //合框
                _resultCanvas = document.getElementById('myCanvas');
                var ctx = _resultCanvas.getContext('2d');
                var image = new Image();
                image.onload = function () {
                    ctx.drawImage(image, 0, 0, 1200, 1200, 0, 0, 1200, 1200);
                    mask.src = 'img/mask.png';
                };
                var mask = new Image();
                mask.onload = function () {
                    ctx.drawImage(mask, 0, 0, 1200, 1200, 0, 0, 1200, 1200);
                };
                image.src = result;

                //顯示下一個步驟
                $('.js-step2').removeClass('active')
                $('.js-step3').addClass('active');
            });
        });
        $('#uploadDownloadCropped').on('click', function () {
            var aLink = document.createElement('a');
            var blob = base64ToBlob(_resultCanvas.toDataURL('image/jpg', 0.8)); //new Blob([content]);

            var evt = document.createEvent("HTMLEvents");
            evt.initEvent('click', true, true);
            aLink.download = 'hhxnike';
            aLink.href = URL.createObjectURL(blob);
            aLink.click();
        });
    }

    function base64ToBlob(code) {
        let parts = code.split(';base64,');
        let contentType = parts[0].split(':')[1];
        let raw = window.atob(parts[1]);
        let rawLength = raw.length;

        let uInt8Array = new Uint8Array(rawLength);

        for (let i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }
        return new Blob([uInt8Array], { type: contentType });
    }
})